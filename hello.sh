#!/bin/bash

origem=$1
destino=$2
valor=$3
if [ $# -ne 3 ]; then
   echo "Parametros faltantes"
   exit

else
 resultado=`curl "https://api.exchangeratesapi.io/latest?base=$origem&symbols=$destino" | jq ".rates.$destino"`
 
 echo "Resultado conversao"
 
 python -c "print $3*$resultado"

fi





